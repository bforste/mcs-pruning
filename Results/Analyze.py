import sqlite3 as sql
# import pystaggrelite3 as pyAgg
import fileinput
import pandas as pd
import pylab as p
import numpy as np
import matplotlib.ticker as ticker
from scipy.interpolate import griddata
import glob
import matplotlib
from sqlalchemy import create_engine # database connection
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d
from matplotlib import *
from matplotlib.lines import Line2D
import os
matplotlib.style.use('ggplot')

from scipy import stats

# Pruners     = ['ACO', 'AIS', 'GA', 'PSO']
Systems     = ['RTS79', 'MRTS', 'RTS96']
Gens        = { 'RTS79': 32, 'MRTS': 32, 'RTS96': 96}
# lineColors  = ["#A00000", "#00 A000", "#5060D0", "#F25900", "#BB00D4"]
# lineMarkers = ['+', 'o', 'x', '^', 'D']
# figureDir   = "..\\Figures\\"


def rankSumTest(treatment1, treatment2):
    
    
    # z_stat, pVal = stats.ranksums(treatment1, treatment2)
    # z_stat, pVal = stats.mannwhitneyu(treatment1.tolist(), treatment2.tolist())
    z_stat, pVal = stats.kruskal(treatment1.tolist(), treatment2.tolist())
    
    # print "MWW RankSum P for treatments 1 and 2 =", p_val
    # MWW RankSum P for treatments 1 and 2 = 0.000983355902735

    return pVal

def analyzeData():
    # print(os.getcwd())
    db = create_engine('sqlite:///' + os.path.dirname(os.path.abspath(__file__)) + '/Results.db')

    df = pd.read_sql_query('SELECT * FROM Samples', db)

    mcs         = df.groupby('Sampler').get_group('MCS')
    mcsBatch    = df.groupby('Sampler').get_group('MCS_OMP_Batch')
    mcsPipeline = df.groupby('Sampler').get_group('MCS_OMP_Pipeline')

    mcs         = mcs.groupby('System')
    mcsBatch    = mcsBatch.groupby(['System', 'numThreads', 'BatchSize'])
    mcsPipeline = mcsPipeline.groupby(['System', 'genThreads', 'classThreads'])


    ###################################### Begin Batch Level Parallelism ######################################
    # Initial
    #pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.4f} +/- {:>6,.4f} {:>6,.4f} +/- {:>6,.4f} {:>6,.4f} {:>2,.2f} {:>2,.2f}"

    # 2 Sig Figs
    # pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.2f} +/- {:>6,.2f} {:>6,.2f} +/- {:>6,.2f} {:>6,.2f} {:>2,.2f} {:>2,.2f}"

    # Without +/-
    # pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.4f} {:>6,.4f} {:>6,.4f} {:>6,.4f} {:>6,.4f} {:>2,.2f} {:>2,.2f}"
    # for system in Systems:
    #     sTime    = mcs.get_group(system)['TotalTime'].mean()
    #     sSamples = mcs.get_group(system)['SamplerIterations'].mean()
        
    #     sRatio = []
    #     gRatio = []
    #     time   = []
    #     for name, group in mcsBatch:
    #          if name[0] == system:

    #             # Is time difference significant?
    #             pTime = rankSumTest(mcs.get_group(system)['TotalTime'].values, group['TotalTime'].values)

    #             # Are the LOLP values calculated similar?
    #             pLolp = rankSumTest(mcs.get_group(system)['SamplerLOLP'].values, group['SamplerLOLP'].values)

    #             # if pTime > 0.05: 
                # if pLolp <= 0.05:
    #                 print pLine.format(name[0], name[1], name[2],
    #                                 group['TotalTime'].mean(), group['TotalTime'].std(),
    #                                 group['SamplerLOLP'].mean(), group['SamplerLOLP'].std(),
    #                                 sTime/group['TotalTime'].mean(),
    #                                 pTime, pLolp)

    #             sRatio.append(sSamples/group['SamplerIterations'].mean())
    #             gRatio.append(Gens[name[0]]/float(name[1]))
    #             time.append(group['TotalTime'].mean())

        # print ""

        

    # print  ""
    # print "###################################################################"
    # print ""
    ###################################### Begin Batch Level Parallelism  Graphs ######################################
    # for system in ["RTS79", "MRTS", "RTS96"]:
    #     nThreads = np.array([name[1] for name, group in mcsBatch if name[0] == system], np.dtype('float64'))
    #     bSize    = np.array([name[2] for name, group in mcsBatch if name[0] == system], np.dtype('float64'))
    #     time     = np.array([group['TotalTime'].mean() for name, group in mcsBatch if name[0] == system], np.dtype('float64'))

    #     sTime    = mcs.get_group(system)['TotalTime'].mean()
    #     speedup  = np.array([sTime/x for x in time], np.dtype('float64'))
    #     eff      = speedup/nThreads

    #     surfacePlot(nThreads, bSize, time,    figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Batch_Time.png")
    #     surfacePlot(nThreads, bSize, speedup, figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Speedup',
    #                 saveToFile = system + "_Batch_Speedup.png")
    #     surfacePlot(nThreads, bSize, eff,     figureTitle="", xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Efficiency',
    #                 saveToFile = system + "_Batch_Eff.png")


    ###################################### Begin Pipeline Level Parallelism ######################################

    # #pLine = "{:s} {:2,d} {:2,d} {:>7,.4f} +/- {:>7,.4f} {:>7,.4f} +/- {:>7,.4f}"
    # pLine = "{:s}, {:2,d}, {:2,d}, {:>7,.4f}, {:>7,.4f}, {:>7,.4f}, {:>7,.4f}"
    pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.4f} {:>6,.4f} {:>6,.4f} {:>6,.4f} {:>6,.4f} {:>2,.2f} {:>2,.2f}"

    #2 Sig Figs
    # pLine = "{:s} {:2,d} {:>6,.0f} {:>7,.2f} +/- {:>6,.2f} {:>6,.2f} +/- {:>6,.2f} {:>6,.2f} {:>2,.2f} {:>2,.2f}"
    for system in ["RTS79", "MRTS", "RTS96"]:
        sTime  = mcs.get_group(system)['TotalTime'].mean()
        sSamples = mcs.get_group(system)['SamplerIterations'].mean()

        # print sSamples

        for name, group in mcsPipeline:
            if name[0] == system:
                # Is time difference significant?
                pTime = rankSumTest(mcs.get_group(system)['TotalTime'].values, group['TotalTime'].values)

                # Are the LOLP values calculated similar?
                pLolp = rankSumTest(mcs.get_group(system)['SamplerLOLP'].values, group['SamplerLOLP'].values)

                # if pTime > 0.05:
                # if pLolp <= 0.05:
                print pLine.format(name[0], name[1], name[2],
                                    group['TotalTime'].mean(), group['TotalTime'].std(),
                                    group['SamplerLOLP'].mean(), group['SamplerLOLP'].std(),
                                    sTime/group['TotalTime'].mean(),
                                    pTime, pLolp)
        print ""


    ###################################### Begin Pipeline Level Parallelism  Graphs ######################################
    # for system in ["RTS79", "MRTS", "RTS96"]:
    #     gThreads  = np.array([name[1] for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))
    #     cThreads  = np.array([name[2] for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))
    #     time      = np.array([group['TotalTime'].mean() for name, group in mcsPipeline if name[0] == system], np.dtype('float64'))

    #     sTime    = mcs.get_group(system)['TotalTime'].mean()
    #     speedup  = np.array([sTime/x for x in time], np.dtype('float64'))
    #     eff      = speedup/(gThreads + cThreads)

    #     surfacePlot(cThreads, gThreads, time,    figureTitle="", xAxisTitle='Class. Threads', yAxisTitle='Gen. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Time.png")
    #     surfacePlot(gThreads, cThreads, speedup, figureTitle="", xAxisTitle='Gen. Threads', yAxisTitle='Class. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Speedup.png")
    #     surfacePlot(gThreads, cThreads, eff,     figureTitle="", xAxisTitle='Gen. Threads', yAxisTitle='Class. Threads', zAxisTitle='Time(s)',
    #                 saveToFile = system + "_Pipeline_Eff.png")

def func(x, pos):  # formatter function takes tick label and tick position
    s = str(x)
    s = s[0:len(s)-2]
    if len(s) > 3:
        s = s[0:len(s)-3] + "," + s[-3:]
    return s

def truncateTable(sql, outFile):
    con = sql.connect(outFile)
    cur = con.cursor()

    cur.execute("DROP TABLE IF EXISTS Samples;")

    cur.execute("CREATE TABLE IF NOT EXISTS Samples(ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                      RunNum INT, Population INT, Generations INT, SuccessStates INT, \
                                      FailureStates INT, StatesSlope DOUBLE, ProbSlope DOUBLE, \
                                      SamplerLOLP DOUBLE, PrunedLOLP DOUBLE, EstimateLOLP DOUBLE, \
                                      PruningTime DOUBLE, StateGenerationTime DOUBLE, SearchTime DOUBLE, \
                                      ClassificationTime DOUBLE, SimulationTime DOUBLE, TotalTime DOUBLE, \
                                      SamplerIterations INT, PruningIterations INT, Collisions INT, \
                                      AvgNumLinesOut INT, LineAdj DOUBLE, AvgNumGensOut DOUBLE, \
                                      PHEVPenetration DOUBLE, PHEVRho DOUBLE, numThreads INT, \
                                      System TEXT DEFAULT 'RTS79', Sampler TEXT DEFAULT 'MCS', \
                                      Classifier TEXT DEFAULT 'OPF', Pruner TEXT DEFAULT 'NONE', \
                                      PruningObjective TEXT DEFAULT 'NONE', LineFailures TEXT DEFAULT 'FALSE',\
                                      MultiObj TEXT DEFAULT 'FALSE', LocalSearch TEXT DEFAULT 'FALSE', \
                                      NegateFitness TEXT DEFAULT 'FALSE', PhevPlacement TEXT DEFAULT 'NONE', \
                                      BatchSize INT, genThreads INT DEFAULT 0, classThreads INT DEFAULT 0)")

    con.commit()

def importData(outFile, inFile, sql, drop = False):
    con = sql.connect(outFile)
    cur = con.cursor()

    if drop == True:
        cur.execute("DROP TABLE IF EXISTS Samples;")

        cur.execute("CREATE TABLE IF NOT EXISTS Samples(ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                      RunNum INT, Population INT, Generations INT, SuccessStates INT, \
                                      FailureStates INT, StatesSlope DOUBLE, ProbSlope DOUBLE, \
                                      SamplerLOLP DOUBLE, PrunedLOLP DOUBLE, EstimateLOLP DOUBLE, \
                                      PruningTime DOUBLE, StateGenerationTime DOUBLE, SearchTime DOUBLE, \
                                      ClassificationTime DOUBLE, SimulationTime DOUBLE, TotalTime DOUBLE, \
                                      SamplerIterations INT, PruningIterations INT, Collisions INT, \
                                      AvgNumLinesOut INT, LineAdj DOUBLE, AvgNumGensOut DOUBLE, \
                                      PHEVPenetration DOUBLE, PHEVRho DOUBLE, numThreads INT, \
                                      System TEXT DEFAULT 'RTS79', Sampler TEXT DEFAULT 'MCS', \
                                      Classifier TEXT DEFAULT 'OPF', Pruner TEXT DEFAULT 'NONE', \
                                      PruningObjective TEXT DEFAULT 'NONE', LineFailures TEXT DEFAULT 'FALSE',\
                                      MultiObj TEXT DEFAULT 'FALSE', LocalSearch TEXT DEFAULT 'FALSE', \
                                      NegateFitness TEXT DEFAULT 'FALSE', PhevPlacement TEXT DEFAULT 'NONE', \
                                      BatchSize INT, genThreads INT DEFAULT 0, classThreads INT DEFAULT 0)")
        print("Samples Table Created.")

    insertSql = "INSERT INTO Samples VALUES(null,"

    fileName = inFile
    for line in fileinput.input(fileName):
        if not fileinput.isfirstline():
            # print "Inserting..."
            sql  = insertSql
            while line.find("  ") != -1:
                line = line.replace("  ", " ")
            line = line.rstrip()
            vals = line.split(" ")

            if len(vals) > 0:
                if len(vals) == 35:
                    vals.insert(34, "None")

                # Fixing Batch Size, genThread, classThread mixup
                if "02_12_2016" in fileName or "02_13_2016" in fileName or "02_14_2016" in fileName:
                    t = vals[37]
                    vals[37] = vals[36]
                    vals[36] = vals[35]
                    vals[35] = t

                for s in vals:
                    sql = (sql + "'" + s + "',")

                sql = sql[:-1] + ")"

            try:
                cur.execute(sql)
                # print fileName + ": Imported line " + str(fileinput.lineno())
            except Exception as e:
                print(e)
                print(fileName)
                print(fileinput.lineno())
                print(len(line.split(" ")))
                print(line)
                print(sql)
                print("")
                raise e

    con.commit()
    con.close()

    print( inFile + " imported.")

def surfacePlot(X, Y, Z, figureTitle, xAxisTitle, yAxisTitle, zAxisTitle, saveToFile="", formatter = ticker.FuncFormatter(func)):

    fig = pyplot.figure(figsize=(15, 8.5))
    ax  = fig.gca(projection='3d')
    xi  = np.linspace(X.min(), X.max(), 100)
    yi  = np.linspace(Y.min(), Y.max(), 100)
    zi  = griddata((X, Y), Z, (xi[None,:], yi[:,None]))

    xig, yig = np.meshgrid(xi, yi)

    # color_list = cm.jet(np.linspace(0, z.max(), int(z.max()))

    surf = ax.plot_surface(xig, yig, zi, cmap=cm.CMRmap, linewidth=0, vmin=Z.min(), vmax=Z.max())
    fig.colorbar(surf, shrink=0.5)

    ax.yaxis.set_major_formatter(formatter)

    ax.set_title(figureTitle)
    ax.set_xlabel(xAxisTitle)
    ax.set_ylabel(yAxisTitle)
    ax.set_zlabel(zAxisTitle)

    if saveToFile == "":
        print("Showing...")
        pyplot.show()
    else:
        print("Saving..." + saveToFile)
        pylab.savefig(saveToFile)

def importAllData(dbFile, sql, pattern="RTS79\\*.csv", truncTable=False):
    files = glob.glob(pattern)
    # print files

    if truncTable == True:
        truncateTable(sql, dbFile)

    # print "Importing " + str(len(files)) + " files"
    for f in files:
        print("Importing " + f)
        importData(dbFile, f, sql)

if __name__ == "__main__":
    dbFile = 'Results.db'

    # importAllData(dbFile, sql, pattern="RTS79/*.csv", truncTable=True)
    # importAllData(dbFile, sql, pattern="MRTS/*.csv")
    # importAllData(dbFile, sql, pattern="RTS96/*.csv")

    analyzeData()
